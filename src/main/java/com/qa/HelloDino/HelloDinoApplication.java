package com.qa.HelloDino;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.boot.web.servlet.support.*;

@SpringBootApplication
public class HelloDinoApplication  extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(HelloDinoApplication.class, args);
	}

}
